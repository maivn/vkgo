package domain

type MarketAlbums struct {
	Response struct {
		Count int64         `json:"count"`
		Items []MarketAlbum `json:"items"`
	} `json:"response"`
}

type MarketAlbum struct {
	Id       int64  `json:"id"`
	OwnerId  int64  `json:"owner_id"`
	Title    string `json:"title"`
	IsMain   bool   `json:"is_main"`
	IsHidden bool   `json:"is_hidden"`
	Count    int64  `json:"count"`
}

type Markets struct {
	Response struct {
		Count int64    `json:"count"`
		Items []Market `json:"items"`
	} `json:"response"`
}

type Market struct {
	Id           int64      `json:"id"`
	OwnerId      int64      `json:"owner_id"`
	Name         string     `json:"name"`
	Title        string     `json:"title"`
	Description  string     `json:"string"`
	Price        Price      `json:"price"`
	Dimensions   Dimensions `json:"dimensions"`
	Weight       int64      `json:"weight"`
	Category     Category   `json:"category"`
	CategoryId   int64      `json:"category_id"`
	ThumbPhoto   string     `json:"thumb_photo"`
	Date         int64      `json:"date"`
	Availability int64      `json:"availability"`
	IsFavorite   bool       `json:"is_favorite"`
	Sku          string     `json:"sku"`
	Photos       Photos     `json:"photos"`
}

type Price struct {
	Amount   string `json:"amount"`
	Currency struct {
		Id   int64  `json:"id"`
		Name string `json:"name"`
	} `json:"currency"`
	OldAmount string `json:"old_amount"`
	Text      string `json:"text"`
}

type Dimensions struct {
	Width  int64 `json:"width"`
	Height int64 `json:"height"`
	Length int64 `json:"length"`
}

type Category struct {
	Id      int64  `json:"id"`
	Name    string `json:"name"`
	Section struct {
		Id   int64  `json:"id"`
		Name string `json:"name"`
	}
}

type Photos []Photo

type Photo struct {
	Id      int64  `json:"id"`
	AlbumId int64  `json:"album_id"`
	OwnerId int64  `json:"owner_id"`
	UserId  int64  `json:"user_id"`
	Text    string `json:"text"`
	Date    int64  `json:"date"`
	Sizes   Sizes  `json:"sizes"`
	Width   int64  `json:"width"`
	Height  int64  `json:"height"`
}

type Sizes []Size

type Size struct {
	Type   string `json:"type"`
	Url    string `json:"url"`
	Width  int64  `json:"width"`
	Height int64  `json:"height"`
}
