package domain

type GroupItems struct {
	Response struct {
		Count int64   `json:"count"`
		Items []Group `json:"items"`
	} `json:"response"`
}

type Groups struct {
	Response []Group `json:"response"`
}

type Group struct {
	Id   int64  `json:"id"`
	Name string `json:"name"`
}
