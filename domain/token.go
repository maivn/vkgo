package domain

type Token struct {
	AccessToken string `json:"access_token"`
	UserId      int64  `json:"user_id"`
	Groups      []struct {
		GroupId     int64  `json:"group_id"`
		AccessToken string `json:"access_token"`
	} `json:"groups"`
	ExpiresIn int64 `json:"expires_in"`
}
