package domain

type ServerItems struct {
	Response struct {
		Code     string   `json:"code"`
		ServerId int64    `json:"server_id"`
		Count    int64    `json:"count"`
		Items    []Server `json:"items"`
	} `json:"response"`
}

type Servers struct {
	Response []Server `json:"response"`
}

type Server struct {
	Id        int64  `json:"id"`
	Title     string `json:"title"`
	CreatorId int64  `json:"creator_id"`
	Url       string `json:"url"`
	SecretKey string `json:"secret_key"`
	Status    string `json:"status"`
}

type ServerSettingsResponse struct {
	Response ServerSettings `json:"response"`
}

type ServerSettings struct {
	ApiVersion string               `json:"api_version"`
	Events     ServerSettingsEvents `json:"events"`
}

type ServerSettingsEvents struct {
	MessageNew                    int64 `json:"message_new"`
	MessageReply                  int64 `json:"message_reply"`
	MessageAllow                  int64 `json:"message_allow"`
	MessageEdit                   int64 `json:"message_edit"`
	MessageDeny                   int64 `json:"message_deny"`
	MessageTypingState            int64 `json:"message_typing_state"`
	PhotoNew                      int64 `json:"photo_new"`
	AudioNew                      int64 `json:"audio_new"`
	VideoNew                      int64 `json:"video_new"`
	WallReplyNew                  int64 `json:"wall_reply_new"`
	WallReplyEdit                 int64 `json:"wall_reply_edit"`
	WallReplyDelete               int64 `json:"wall_reply_delete"`
	WallReplyRestore              int64 `json:"wall_reply_restore"`
	WallPostNew                   int64 `json:"wall_post_new"`
	WallRepost                    int64 `json:"wall_repost"`
	BoardPostNew                  int64 `json:"board_post_new"`
	BoardPostEdit                 int64 `json:"board_post_edit"`
	BoardPostRestore              int64 `json:"board_post_restore"`
	BoardPostDelete               int64 `json:"board_post_delete"`
	PhotoCommentNew               int64 `json:"photo_comment_new"`
	PhotoCommentEdit              int64 `json:"photo_comment_edit"`
	PhotoCommentDelete            int64 `json:"photo_comment_delete"`
	PhotoCommentRestore           int64 `json:"photo_comment_restore"`
	VideoCommentNew               int64 `json:"video_comment_new"`
	VideoCommentEdit              int64 `json:"video_comment_edit"`
	VideoCommentDelete            int64 `json:"video_comment_delete"`
	VideoCommentRestore           int64 `json:"video_comment_restore"`
	MarketCommentNew              int64 `json:"market_comment_new"`
	MarketCommentEdit             int64 `json:"market_comment_edit"`
	MarketCommentDelete           int64 `json:"market_comment_delete"`
	MarketCommentRestore          int64 `json:"market_comment_restore"`
	MarketOrderNew                int64 `json:"market_order_new"`
	MarketOrderEdit               int64 `json:"market_order_edit"`
	PollVoteNew                   int64 `json:"poll_vote_new"`
	GroupJoin                     int64 `json:"group_join"`
	GroupLeave                    int64 `json:"group_leave"`
	GroupChangeSettings           int64 `json:"group_change_settings"`
	GroupChangePhoto              int64 `json:"group_change_photo"`
	GroupOfficersEdit             int64 `json:"group_officers_edit"`
	UserBlock                     int64 `json:"user_block"`
	UserUnblock                   int64 `json:"user_unblock"`
	LeadFormsNew                  int64 `json:"lead_forms_new"`
	LikeAdd                       int64 `json:"like_add"`
	LikeRemove                    int64 `json:"like_remove"`
	MessageEvent                  int64 `json:"message_event"`
	DonutSubscriptionCreate       int64 `json:"donut_subscription_create"`
	DonutSubscriptionProlonged    int64 `json:"donut_subscription_prolonged"`
	DonutSubscriptionCancelled    int64 `json:"donut_subscription_cancelled"`
	DonutSubscriptionPriceChanged int64 `json:"donut_subscription_price_changed"`
	DonutSubscriptionExpired      int64 `json:"donut_subscription_expired"`
	DonutMoneyWithdraw            int64 `json:"donut_money_withdraw"`
	DonutMoneyWithdrawError       int64 `json:"donut_money_withdraw_error"`
}
