package domain

type WebHook struct {
	Type    string      `json:"type"`
	EventId string      `json:"event_id"`
	V       string      `json:"v"`
	Object  interface{} `json:"object"`
	GroupId int64       `json:"group_id"`
}
