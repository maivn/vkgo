package client

import (
	"fmt"
	"gitlab.com/maivn/vkgo/domain"
	"net/http"
	"net/url"
)

const (
	GetTokenResource = "/access_token"
)

type Token struct {
	VkHttpClient *VkHttpClient
}

func NewToken(vkHttpClient *VkHttpClient) *Token {
	return &Token{VkHttpClient: vkHttpClient}
}

func (t *Token) GetByCode(code string) (*domain.Token, error) {
	option := OptionsRequest{
		Host: "oauth.vk.com",
		Path: GetTokenResource,
		Query: url.Values{
			"client_id":     []string{t.VkHttpClient.App.ClientId},
			"client_secret": []string{t.VkHttpClient.App.ClientSecret},
			"redirect_uri":  []string{t.VkHttpClient.App.RedirectUri},
			"code":          []string{code},
		},
	}
	fmt.Println("option.Body", option.Body)

	var token domain.Token
	err := t.VkHttpClient.request(http.MethodPost, &option, &token)
	if err != nil {
		return nil, err
	}

	if token.AccessToken != "" {
		t.VkHttpClient.AccessToken = token.AccessToken
	} else if len(token.Groups) > 0 && token.Groups[0].AccessToken != "" {
		t.VkHttpClient.AccessToken = token.Groups[0].AccessToken
	}

	return &token, nil
	//return t.VkHttpClient.GetTokenByCode(code)
}
