package client

import (
	"fmt"
	"gitlab.com/maivn/vkgo/domain"
	"net/http"
	"net/url"
)

type Market struct {
	VkHttpClient *VkHttpClient
}

func NewMarket(vkHttpClient *VkHttpClient) *Market {
	return &Market{VkHttpClient: vkHttpClient}
}

func (m *Market) GetAlbums(values url.Values) (*domain.MarketAlbums, error) {
	option := OptionsRequest{
		Host:  "api.vk.com",
		Path:  "/method/market.getAlbums",
		Query: values,
	}

	var marketAlbums domain.MarketAlbums

	err := m.VkHttpClient.request(http.MethodPost, &option, &marketAlbums)
	if err != nil {
		return nil, err
	}

	return &marketAlbums, nil
}

func (m *Market) AddAlbum(values url.Values) (int64, error) {
	option := OptionsRequest{
		Host:  "api.vk.com",
		Path:  "/method/market.addAlbum",
		Query: values,
	}

	var result struct {
		Response struct {
			MarketAlbumId int64 `json:"market_album_id"`
		}
	}

	err := m.VkHttpClient.request(http.MethodPost, &option, &result)
	if err != nil {
		return 0, err
	}

	return result.Response.MarketAlbumId, nil
}

func (m *Market) Get(values url.Values) (*domain.Markets, error) {
	option := OptionsRequest{
		Host:  "api.vk.com",
		Path:  "/method/market.get",
		Query: values,
	}

	var markets domain.Markets

	err := m.VkHttpClient.request(http.MethodPost, &option, &markets)
	if err != nil {
		return nil, err
	}

	return &markets, nil
}

func (m *Market) Delete(ownerId, itemId int64) (*int64, error) {
	option := OptionsRequest{
		Host: "api.vk.com",
		Path: "/method/market.delete",
		Query: url.Values{
			"owner_id": []string{fmt.Sprintf("%d", ownerId)},
			"item_id":  []string{fmt.Sprintf("%d", itemId)},
		},
	}

	var result struct {
		Response int64 `json:"response"`
	}
	err := m.VkHttpClient.request(http.MethodPost, &option, &result)
	if err != nil {
		return nil, err
	}

	return &result.Response, nil
}

func (m *Market) Add(values url.Values) error {
	option := OptionsRequest{
		Host:  "api.vk.com",
		Path:  "/method/market.add",
		Query: values,
	}
	var result struct {
		Response string `json:"response"`
	}
	err := m.VkHttpClient.request(http.MethodPost, &option, &result)
	if err != nil {
		return err
	}

	fmt.Println("result", result.Response)
	return nil
}
