package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/maivn/vkgo/domain"
	"io"
	"net/http"
	"net/url"
)

const (
	DefaultContentType  = "application/json"
	DefaultAccept       = DefaultContentType
	DefaultCacheControl = "no-cache"
)

type OptionsRequest struct {
	Host  string
	Path  string
	Query url.Values
	Body  interface{}
}

type getTokenRequest struct {
	ClientId     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
	RedirectUri  string `json:"redirect_uri"`
	Code         string `json:"code"`
}

type VkHttpClient struct {
	AccessToken string      `json:"access_token"`
	App         *domain.App `json:"app"`
}

func NewVkHttpClient(accessToken string, app *domain.App) *VkHttpClient {
	return &VkHttpClient{
		AccessToken: accessToken,
		App:         app,
	}
}

/*
func (h *VkHttpClient) GetTokenByCode(code string) (*domain.Token, error) {
	option := OptionsRequest{
		Host: "oauth.vk.com",
		Path: GetTokenResource,
		Query: url.Values{
			"client_id":     []string{h.App.ClientId},
			"client_secret": []string{h.App.ClientSecret},
			"redirect_uri":  []string{h.App.RedirectUri},
			"code":          []string{code},
		},
	}
	fmt.Println("option.Body", option.Body)

	var token domain.Token
	err := h.request(http.MethodPost, &option, &token)
	if err != nil {
		return nil, err
	}
	h.Token = &token

	if h.callback != nil {
		h.callback(h.Token)
	}

	return &token, nil
}
*/
func (h *VkHttpClient) request(method string, optionsRequest *OptionsRequest, result interface{}) (err error) {
	if optionsRequest.Query == nil {
		optionsRequest.Query = url.Values{}
	}
	optionsRequest.Query.Add("v", h.App.Version)

	u := url.URL{
		Scheme:   "https",
		Host:     optionsRequest.Host,
		Path:     optionsRequest.Path,
		RawQuery: optionsRequest.Query.Encode(),
	}

	jsonBody, err := json.Marshal(optionsRequest.Body)
	if err != nil {
		return
	}

	//fmt.Println(string(jsonBody))
	req, err := http.NewRequest(method, u.String(), bytes.NewReader(jsonBody))
	if err != nil {
		return
	}

	for k, v := range getHeaders(h.AccessToken) {
		req.Header.Set(k, v)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return
	}

	fmt.Println(" resp.StatusCode ", resp.StatusCode)
	if resp.StatusCode >= 400 {
		d, _ := json.Marshal(resp.Body)
		fmt.Println(string(d))
		//todo handle error
		/*var errorResponse domain.ErrorResponse
		decoder := jsoniter.NewDecoder(resp.Body)
		err = decoder.Decode(&errorResponse)

		fmt.Println("errorResponse", errorResponse)
		if err != nil {
			return err
		}
		if errorResponse.Status != 0 {
			return &errorResponse
		}*/
		return
	}

	if result != nil && resp.StatusCode == 200 {
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			return err
		}

		fmt.Println("body", string(body))

		err = json.Unmarshal(body, result)
		if err != nil {
			return err
		}
	}
	return
}

func getHeaders(accessToken string) map[string]string {
	headers := map[string]string{
		"Accept":        DefaultAccept,
		"Cache-Control": DefaultCacheControl,
		"Content-Type":  DefaultContentType,
	}
	if accessToken != "" {
		headers["Authorization"] = fmt.Sprintf("Bearer %s", accessToken)
	}
	return headers
}
