package client

import (
	"gitlab.com/maivn/vkgo/domain"
	"net/http"
	"net/url"
)

type Group struct {
	VkHttpClient *VkHttpClient
}

func NewGroup(vkHttpClient *VkHttpClient) *Group {
	return &Group{VkHttpClient: vkHttpClient}
}

func (g *Group) Get(values url.Values) (*domain.GroupItems, error) {
	option := OptionsRequest{
		Host:  "api.vk.com",
		Path:  "/method/groups.get",
		Query: values,
	}

	var groupItems domain.GroupItems

	err := g.VkHttpClient.request(http.MethodPost, &option, &groupItems)
	if err != nil {
		return nil, err
	}

	return &groupItems, nil
}

func (g *Group) GetById(values url.Values) (*domain.Groups, error) {
	option := OptionsRequest{
		Host:  "api.vk.com",
		Path:  "/method/groups.getById",
		Query: values,
	}

	var groups domain.Groups

	err := g.VkHttpClient.request(http.MethodPost, &option, &groups)
	if err != nil {
		return nil, err
	}

	return &groups, nil
}

func (g *Group) GetCallbackServers(values url.Values) (*domain.ServerItems, error) {
	option := OptionsRequest{
		Host:  "api.vk.com",
		Path:  "/method/groups.getCallbackServers",
		Query: values,
	}

	var serverItems domain.ServerItems

	err := g.VkHttpClient.request(http.MethodPost, &option, &serverItems)
	if err != nil {
		return nil, err
	}

	return &serverItems, nil
}

func (g *Group) AddCallbackServer(values url.Values) (int64, error) {
	option := OptionsRequest{
		Host:  "api.vk.com",
		Path:  "/method/groups.addCallbackServer",
		Query: values,
	}

	var serverItems domain.ServerItems

	err := g.VkHttpClient.request(http.MethodPost, &option, &serverItems)
	if err != nil {
		return 0, err
	}

	return serverItems.Response.ServerId, nil
}

func (g *Group) GetCallbackConfirmationCode(values url.Values) (string, error) {
	option := OptionsRequest{
		Host:  "api.vk.com",
		Path:  "/method/groups.getCallbackConfirmationCode",
		Query: values,
	}

	var serverItems domain.ServerItems

	err := g.VkHttpClient.request(http.MethodPost, &option, &serverItems)
	if err != nil {
		return "", err
	}

	return serverItems.Response.Code, nil
}

/*
func (g *Group) GetCallbackSettings(values url.Values) (*domain.ServerSettingsResponse, error) {
	option := OptionsRequest{
		Host:  "api.vk.com",
		Path:  "/method/groups.getCallbackSettings",
		Query: values,
	}

	var serverSettings domain.ServerSettingsResponse

	err := g.VkHttpClient.request(http.MethodPost, &option, &serverSettings)
	if err != nil {
		return nil, err
	}

	return &serverSettings, nil
}


func (g *Group) GetCallbackSettings(values url.Values) (*domain.ServerSettingsResponse, error) {
	option := OptionsRequest{
		Host:  "api.vk.com",
		Path:  "/method/groups.getCallbackSettings",
		Query: values,
	}

	var serverSettings domain.ServerSettingsResponse

	err := g.VkHttpClient.request(http.MethodPost, &option, &serverSettings)
	if err != nil {
		return nil, err
	}

	return &serverSettings, nil
}
*/
