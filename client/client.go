package client

import (
	"gitlab.com/maivn/vkgo/domain"
)

type ApiClient struct {
	Token  *Token  `json:"token"`
	User   *User   `json:"user"`
	Group  *Group  `json:"group"`
	Market *Market `json:"market"`
	Photo  *Photo  `json:"photo"`
}

func NewApiClient(accessToken string, app *domain.App) *ApiClient {
	vkHttpClient := NewVkHttpClient(accessToken, app)
	return &ApiClient{
		Token:  NewToken(vkHttpClient),
		User:   NewUser(vkHttpClient),
		Group:  NewGroup(vkHttpClient),
		Market: NewMarket(vkHttpClient),
		Photo:  NewPhoto(vkHttpClient),
	}
}
