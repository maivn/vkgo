package client

import (
	"gitlab.com/maivn/vkgo/domain"
	"net/http"
	"net/url"
)

type User struct {
	VkHttpClient *VkHttpClient
}

func NewUser(vkHttpClient *VkHttpClient) *User {
	return &User{VkHttpClient: vkHttpClient}
}

func (u *User) Get(values url.Values) (*domain.Users, error) {
	option := OptionsRequest{
		Host:  "api.vk.com",
		Path:  "/method/users.get",
		Query: values,
	}

	var users domain.Users

	err := u.VkHttpClient.request(http.MethodPost, &option, &users)
	if err != nil {
		return nil, err
	}

	return &users, nil
}
