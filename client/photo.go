package client

import (
	"gitlab.com/maivn/vkgo/domain"
	"net/http"
	"net/url"
)

type Photo struct {
	VkHttpClient *VkHttpClient
}

func NewPhoto(vkHttpClient *VkHttpClient) *Photo {
	return &Photo{VkHttpClient: vkHttpClient}
}

func (m *Photo) GetMarketUploadServer(values url.Values) (string, error) {
	option := OptionsRequest{
		Host:  "api.vk.com",
		Path:  "/method/photos.getMarketUploadServer",
		Query: values,
	}
	var result struct {
		Response struct {
			UploadUrl string `json:"upload_url"`
		} `json:"response"`
	}
	err := m.VkHttpClient.request(http.MethodPost, &option, &result)
	if err != nil {
		return "", err
	}

	return result.Response.UploadUrl, nil
}

func (m *Photo) SaveMarketPhoto(values url.Values) (*domain.Photos, error) {
	option := OptionsRequest{
		Host:  "api.vk.com",
		Path:  "/method/photos.saveMarketPhoto",
		Query: values,
	}

	var result struct {
		Response domain.Photos `json:"response"`
	}
	err := m.VkHttpClient.request(http.MethodPost, &option, &result)
	if err != nil {
		return nil, err
	}

	return &result.Response, nil
}
